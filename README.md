# Blaseball Records

The repository for http://blaseball-records.com.

## Contributing

Fork the repository, make your changes, and submit a pull request. I'll manually review and merge your request. Messaging me on Discord and letting me know about your PR is also greatly appreciated, though not strictly *necessary*.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.