---
title: Player Records
---

Many more player records can be found at [Blaseball Reference](https://blaseball-reference.com/leaders), this is just for more niche ones that BR doesn't (or can't) track.

### Inning

Record | Current Record | Recordholder | Record Earned
:-:|:-:|:-:|:-:
Most Hits | 
Most RBIs | 
Most Home Runs | 
Most Runs Given Up | 

### Game

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Hits | 15 | Kelvin Drumsolo (Core Mechanics) | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
Most RBIs | 22 | Kelvin Drumsolo (Core Mechanics) | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
Most Home Runs | 5 | Slosh Truk (Atlantis Georgias) | [Season 19, Day 20](https://reblase.sibr.dev/game/dd8a569d-63a4-493a-863b-5cac234e2676)
Most At-Bats | 29 | Kelvin Drumsolo (Core Mechanics) | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
Most Selves Batted In | 3 | Kelvin Drumsolo (Core Mechanics) | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
Most Strikeouts Pitched in a Perfect Game | 19 | Nerd Pacheco (Philly Pies) | [Season 18, Day 90](https://reblase.sibr.dev/game/9e4ca8a7-49c9-4a41-ac25-c29658f5572b)

### Season

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Hits | 
Most RBIs |  
Most Home Runs | 70 | Mcdowell Mason (LA Unlimited Tacos) | [Season 18, Day 97](https://reblase.sibr.dev/game/2216cf23-d9a6-43c0-8e76-a40a45385ba6#1f07780a-4d7d-fe1f-6274-9ee923deefde)
Most Wins | 1,113* | Randy Castillo (Hades Tigers) | [Season 19](https://reblase.sibr.dev/season/15) (Ongoing)
Worst Record (Pitcher) | 0-20 | Parker Parra (Boston Flowers) | [Season 15](https://reblase.sibr.dev/season/15)
Most At-Bats | 

\*Does not currently count Postseason wins

### Career

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most At-Bats | 
Most Plate Appearances | 
Longest Plate Appearance | 116 Pitches | Chorby Short | [Season 12, Day 1, Inning 2](https://reblase.sibr.dev/game/f316424b-a5c5-48eb-ae1e-c2cfa6bde4bb#aa8e55b9-92b9-73a2-0021-7c7280ca2d6c)
Most Consecutive Foul Balls | 112 | Chorby Short | [Season 12, Day 1, Inning 2](https://reblase.sibr.dev/game/f316424b-a5c5-48eb-ae1e-c2cfa6bde4bb#aa8e55b9-92b9-73a2-0021-7c7280ca2d6c)
Most Consecutive Ground Outs | | 
Most Consecutive Games Pitched | 201 | Sexton Wheerer (Unlimited Tacos) | [Season 8, Day 99]() to [Season 11, Day 1]()
Most Consecutive Games Played | 2,108+ Games (Ongoing) | Randy Castillo (Hades Tigers) | [Season 1, Day 1]() to Season 20, Day 88 (Ongoing)
Most Perfect Games Pitched | 3 | Theodore Cervantes (New York Millennials), Burke Gonzales (Mexico City Wild Wings) (Tie) | [Season 16, Day 91](https://reblase.sibr.dev/game/73e63c5d-0a48-455c-ac7d-1d860af51f03) and [Season 17, Day 21](https://reblase.sibr.dev/game/050c8510-c53f-4576-9ced-5f8aa56a1ae1)
Lowest Combined Star Total | 0.0 | Chorby Soul (Seattle Garages) | [Season 15, Day 7](https://reblase.sibr.dev/game/e5db1298-bba0-4aa4-bd11-3d163d0366dd)
Highest Combined Star Total | 28.3 | Winnie Hess (Kansas City Breath Mints) | [Season 19, Day 96](https://reblase.sibr.dev/game/cf9ea5fb-c132-4dcb-a353-24d72034f130)
Longest Win Streak | 21 | Michelle Sportsman (LA Unlimited Tacos) | [Season 20](https://reblase.sibr.dev/season/15)

### Weather

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Allergic Reactions | 3 | Declan Suzanne (Chicago Firefighters) | [Season 12, Day 42](https://reblase.sibr.dev/game/d85e374c-3d61-4928-8864-59ebc51a9c0b#3b8f36a0-cc0e-5535-d1dc-c7bc66c7a369)
Shortest Time Spent Shelled* | 44 Days | Rodriguez Internet (Kansas City Breath Mints) | [Season 17, Day 5](https://reblase.sibr.dev/game/56e6d430-fc6c-4b31-98b9-f85ee0893f85#b31cde0f-df3c-d8d8-794e-a74f3fc701ba) to [Season 17, Day 49](https://reblase.sibr.dev/game/cb7dd13c-bf04-4bb1-a53a-cf478bc2e26c#bc4a4853-d7a6-477c-0d42-b6469b453952)
Longest Time Spent Shelled* | 
Incineration Attempts | 
Most Umpires Incinerated | 2 | Raúl Leal (fmr. Miami Dale) | [Season 9, Election](https://www.blaseball.wiki/w/Season_9#Blessings_2)
Most Deaths | 2 | Sebastian Telephone (fmr. Dallas Steaks) | [Season 10, Day X](https://reblase.sibr.dev/bossfight/9bb560d9-4925-4845-ad03-26012742ee23)
Most Consumers Fended Off | 4 | Forrest Best (Dallas Steaks) | *When?*
Shortest Time Spent Elsewhere* | 5 Plays | Juice Collins (Hawai'i Fridays) | [Season 14, Day 10](https://reblase.sibr.dev/game/d38669da-05c0-4461-89f4-418f66bd4a8a#1ff2e307-0987-ed38-6907-e9f166679029)
Longest Time Spent Elsewhere* | 5 Seasons, 15 Days+ | Neerie McCloud (Atlantis Georgias) | Ongoing (Started [Season 13, Day 84](https://reblase.sibr.dev/game/4c87a7c4-05e7-43d9-973b-cf97361713d5#931a8e3a-8658-adb0-864f-0cfedcb9ad88))
Longest Time with Triple Threat | 4 Seasons | Lou Roseheart (Chicago Firefighters) | Season 12 - 15

\*Does not include echoed instances of this modifier

### Misc

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Lowest Fate Number | 
Highest Fate Number | 
Longest Soulscream | 148,731 Characters | Chorby Soul | Season 2, Election
Shortest Name | 3 Characters | NaN
Longest Name | 22 Characters | Nolanestophia Patterson
Shortest Lifespan | 5.3 Innings | Kiki Familia (Canada Moist Talkers) | [Season 7, Day 33](https://reblase.sibr.dev/game/a4002683-9b4c-4dae-bbd4-44370d8dd17c)
Shortest Career | Did Not Play | Wyatt Mason XI | [Season 14, Day 77](https://reblase.sibr.dev/game/993aa7f0-cde5-4665-830e-12544084404d)
Longest Career | 19 Seasons | All Season 1 Players | N/A
Shortest Time between Incineration and Haunting | 7 Innings | Sutton Bishop (fmr. Hellmouth Sunbeams) | [Season 13, Day 98, Inning 11 (Bottom)](https://reblase.sibr.dev/game/ed28e440-672c-4893-8acb-688007e5144c#885a62d0-cc77-b263-dc12-264d4f808cd9) to [Season 13, Day 99, Inning 7 (Bottom)](https://reblase.sibr.dev/game/361a22c8-43bb-4865-9252-24a7fe184339#7ade9802-e084-f58d-aa91-657007c4a17c)