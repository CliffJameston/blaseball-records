---
title: Lowest Winning Score
---

Record | Held By | Record Earned
:-:|:-:|:-:
-3 | Kansas City Breath Mints | [Season 20, Day 2](https://reblase.sibr.dev/game/2864826b-0174-437c-8f3e-09856f5fd43f)
-1 | Charleston Shoe Thieves | [Season 19, Day 95](https://reblase.sibr.dev/game/5183b03d-b4ec-4c1f-a38a-e061368011b4)