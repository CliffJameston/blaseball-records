---
title: Team Incineration Counts
---

Team | Incinerations | Last Incineration Date
-|-|-
Canada Moist Talkers | 11
Boston Flowers | 9
Charleston Shoe Thieves | 8
Seattle Garages | 7
Hades Tigers | 7
Baltimore Crabs | 6
Dallas Steaks | 6
New York Millennials | 6
Philly Pies | 6
Breckenridge Jazz Hands | 6
Miami Dale | 6
Hawai'i Fridays | 5
Yellowstone Magic | 5
Mexico City Wild Wings | 5
Houston Spies | 5
Kansas City Breath Mints | 4
Chicago Firefighters | 4
Hellmouth Sunbeams | 4
San Francisco Lovers | 2
Atlantis Georgias | 2
Core Mechanics | 2
LA Unlimited Tacos | 2
Ohio Worms | 2
Tokyo Lift | 1