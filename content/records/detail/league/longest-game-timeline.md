---
title: Longest Game Records Progression
---

## Innings

- 12 innings. Moist Talkers - Jazz Hands, Season 1 Day 1, 5 - 6
- 14 innings. Breath Mints - Lovers, Season 1 Day 24, 7 - 9
- 17 innings. Tigers - Wild Wings, Season 1 Day 38, 4 - 7
- 20 innings. Crabs - Jazz Hands, Season 2 Day 52, 8 - 6
- 22 innings. Flowers - Steaks, Season 4 Day 21, 7 - 6
- 24 innings. Tacos - Flowers, Season 4 Day 99, 1 - 2 
- 28 innings. Mechanics - Fridays, Season 16 Day 36, 2 - 4

## Time

- (unknown early games, data lost)
- 52:08, 20 innings. Crabs - Jazz Hands, Season 2 Day 52, 8 - 6
- ~55:00, 15 innings. Shoe Thieves - Tacos, Season 3 Day 73, 20 - 13 *and* 17 - 14 (The Grand Unslam, exact time unknowable)
- 58:16, 22 innings. Flowers - Steaks, Season 4 Day 21, 7 - 6
- 65:23, 24 innings. Tacos - Flowers, Season 4 Day 99, 1 - 2
- 67:53, 24 innings. Dale - Worms, Season 18 Day 12, 5 - 4