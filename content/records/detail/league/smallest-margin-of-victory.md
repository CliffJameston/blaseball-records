---
title: Smallest Margin of Victory
---

- Firefighters - Spies, Season 13 Day 54, 2.1 - 2
- Lift - Firefighters, Season 14 Day 78, 4 - 4.1
- Fridays - Millennials, Season 17 Day 47, 0.1 - 0
- Garages - Shoe Thieves, Season 18 Day 1, 8.1 - 8
- Magic - Mechanics, Season 18 Day 54, 2.9 - 3
- Mechanics - Steaks, Season 19 Day 9, 9.1 - 9
- Mechanics - Moist Talkers, Season 19 Day 58, 5 - 5.1