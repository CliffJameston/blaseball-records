---
title: Record Descriptions
toc: true
---
Many of the records here should be fairly self-explanatory, but in case you still have questions about what exactly makes up each record, this page should be your first stop.