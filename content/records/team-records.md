---
title: Team Records
---

### Single Game

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Lowest Final Score | -12 | Dallas Steaks | [Season 19, Day 95](https://reblase.sibr.dev/game/5183b03d-b4ec-4c1f-a38a-e061368011b4)
Highest Final Score | 30 | Hades Tigers | [Season 19, Day 50](https://reblase.sibr.dev/game/73aaf7cd-aa85-48b0-ac57-31cc4ac55661)
[Lowest Winning Score](/records/detail/team/lowest-winning-score) | -3 | Kansas City Breath Mints | [Season 20, Day 2](https://reblase.sibr.dev/game/2864826b-0174-437c-8f3e-09856f5fd43f)
Highest Losing Score | 
Most Runs Scored | 35 | LA Unlimited Tacos | [Season 11, Day 36](https://reblase.sibr.dev/game/a238e10f-df74-4ee8-a5c5-60dc5b35c734)
Most Wins | 3 | LA Unlimited Tacos | [Season 11, Day 36](https://reblase.sibr.dev/game/a238e10f-df74-4ee8-a5c5-60dc5b35c734)
Worst Shame (Final Record) | 7 - 15 | Canada Moist Talkers | [Season 14, Day 78](https://reblase.sibr.dev/game/6692ad15-99de-4989-bd7e-0104bf86eeb5)
Worst Shame (Runs Allowed) |  |  | 
Largest Upset | 20% | Unlimited Tacos | [Season 5, Day 5](https://reblase.sibr.dev/game/93340d15-1ecf-41bb-b09e-89be51df57c4)
Largest Shutout (Runs) | 29.1 - 0 | Core Mechanics (Jolene Willowtree) | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
Largest Shutout (Final Score) | 21 - 0 | Philly Pies (Bright Zimmerman) | [Season 1, Day 74](https://reblase.sibr.dev/game/c412869e-7d0a-4756-a455-4eb6d015b0c6)

### Seasonal

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Hits | 
Most Runs | 
Most Wins | 80 | Baltimore Crabs | [Season 6](https://reblase.sibr.dev/season/6)
Worst Regular Season Record (Season Champion) | 46 - 53 | Tokyo Lift | [Season 19](https://reblase.sibr.dev/season/19)
Best Regular Season Record (Season Champion) | 80 - 19 | Baltimore Crabs | [Season 6](https://reblase.sibr.dev/season/6)
Worst Regular Season Record (Non-Season Champion) | 21 - 78 | Hawai'i Fridays | [Season 9](https://reblase.sibr.dev/season/9)
Best Regular Season Record (Non-Season Champion) | 80 - 19 | Philly Pies | [Season 18](https://reblase.sibr.dev/season/18)
Most Deaths | 7 | Canada Moist Talkers | [Season 7](https://reblase.sibr.dev/season/7)

### All Time

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Hits | 
Most Runs | 
Most Wins | 
Most Deaths | 11 | Canada Moist Talkers | [Season 14, Day 87](https://reblase.sibr.dev/game/4eee638f-4b07-4471-841d-f50092d25a3c#61a5a106-5ac3-490d-8782-246fcde90a7f)
Shortest Lineup | 
Most Active Original Roster Players (League-Wide) | | |
Most Active Original Roster Players (Still on Team) | | |
Longest Losing Streak | 29 Games | New York Millennials | [Season 16](https://reblase.sibr.dev/season/16) Day 59 - 88
Longest Winning Streak | 
Fastest Party Time Speedrun | 67 Days | Ohio Worms | [Season 13](https://reblase.sibr.dev/season/13)
Fastest Playoffs Speedrun | 
Most Playoff Appearances | 13 | Hades Tigers | [Season 18](https://reblase.sibr.dev/season/18)
Longest Playoff Drought |
Most Championship Appearances | 5 | Baltimore Crabs | [Season 13](https://reblase.sibr.dev/season/13)
Most ILB Championships | 4 | Baltimore Crabs | [Season 13](https://reblase.sibr.dev/season/13)
Longest Time between First Playoff Appearance and First ILB Championship* | 15 Seasons | Dallas Steaks | [Season 16](https://reblase.sibr.dev/season/16)

\* Does not include teams with playoff appearances but no championships, does not include Wild Card round.