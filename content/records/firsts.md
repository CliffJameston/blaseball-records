---
title: Firsts
menu: main
weight: 30
---

# Historical Firsts

First day with multiple incinerations

First 1+ hour game
- Season 4, Day 99 - Los Angeles Tacos vs Boston Flowers (65:23, 24 Innings)

First negative score
- Season 8, Day 15 - Canada Moist Talkers vs Mexico Wild Wings (-1 - 1)

First double shutout
- Season 8, Day 16 - Chicago Firefighters vs San Francisco Lovers (-1 - 0)

First Pentaslam
- Season 10, Day 7 - Simon Haley (Charleston Shoe Thieves)

First Spillover game
- Season 10, Day 27 - Charleston Shoe Thieves vs Philly Pies (64:35, 19 Innings)

First non-integer score
- Season 11, Day 48 - New York Millennials vs San Francisco Lovers (5.2 - 0)

First negative non-integer score
- Season 12, Day 33 - Boston Flowers vs Tokyo Lift (4.7 - -0.6)

First day with more than one perfect game
- Season 17, Day 10
  - Conrad Vaughan (New York Millennials) vs Inky Rutledge (Yellowstone Magic) (0 - 2)
  - Sosa Hayes (Houston Spies) vs Sixpack Santiago (Miami Dale) (0 - 4)

First player to steal home twice in a single inning
- Season 17, Day 40 - Marquez Clark

First team to party without being eliminated from the playoffs
- Season 17, Day 94 - Miami Dale

First extra-inning perfect game
- Season 18, Day 15 - Winnie Hess (Kansas City Breath Mints) vs Sandie Carver (New York Millennials) (1 - 0)

First Hype
- Season 19, Day 1 - Houston Spies vs Atlantis Georgias

First player to return from Elsewhere completely Scattered
- Season 19, Day 85 - Steph Weeks (Breckenridge Jazz Hands)

First game with a negative winning score
- Season 19, Day 95 - Charleston Shoe Thieves vs Dallas Steaks (-1 - -12)

First non-shutout perfect game
- Season 19, Day 95 - LA Unlimited Tacos (Yummy Elliott) vs Boston Flowers (Chorby Short) (6 - 1)

First player to hit for a natural and reverse cycle
- *When?* - Comfort Septemberish