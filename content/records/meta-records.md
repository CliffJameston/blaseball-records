---
title: Meta Records
---

These records are about the records themselves.

Record | Original Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Shortest Held Record | Longest 9-Inning Game | 
Longest Held Record | Largest Shutout (Final Score) | Philly Pies (Bright Zimmerman) | [Season 1, Day 74](https://reblase.sibr.dev/game/c412869e-7d0a-4756-a455-4eb6d015b0c6)