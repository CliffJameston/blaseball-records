---
title: League-Wide Records
---

Record | Current Record | Held By | Record Earned
:-:|:-:|:-:|:-:
Most Runs Scored in One Play | 6 | Engine Eberhardt (Tokyo Lift) | [Season 14, Day 77, Inning 3](https://reblase.sibr.dev/game/1d3aa608-3ff9-4446-b6ba-068defd9c983#6015b73d-9599-2e88-5ddf-fcb1a81d8ca3)
Shortest 9-Inning Game (IRL Time) | 17:44 | Boston Flowers, Mexico City Wild Wings | [Season 17, Day 49](https://reblase.sibr.dev/game/4562e489-b778-43b6-9c85-84af382aac1a)
Longest 9-Inning Game (IRL Time) | 57:15 | Dallas Steaks, Core Mechanics | [Season 19, Day 69](https://reblase.sibr.dev/game/d55e620b-723c-4fb8-acca-41075a8b2823)
[Shortest Game (IRL Time, excluding Day X)](/records/detail/league/games-under-18-minutes) | 17:44 | Boston Flowers, Mexico City Wild Wings | [Season 17, Day 49](https://reblase.sibr.dev/game/4562e489-b778-43b6-9c85-84af382aac1a)
Longest Game (IRL Time) | 67:53 | Miami Dale, Ohio Worms | [Season 18, Day 12](https://reblase.sibr.dev/game/bac30156-624e-4171-8f60-d660855b3fd5)
Shortest Game (Innings) | 9 | Many Games | N/A
Longest Game (Innings) | 28 | Core Mechanics, Hawaii Fridays | [Season 16, Day 36](https://reblase.sibr.dev/game/ae567408-7cb0-4523-aa86-9a12c1fa063c)
Single Season Hits | | Season # | 
Single Season Runs | | Season # | 
Single Season Perfect Games | | [Season 16](https://reblase.sibr.dev/season/16)
Lowest Total Season Wins | 990 | Seasons 1-10 (Excludes Season 3) | 
Highest Total Season Wins | 1241 | [Season 20](https://reblase.sibr.dev/season/20)
Highest Average Team Wins | 1241 | [Season 20](https://reblase.sibr.dev/season/20)
Lowest Combined Runs Scored | 
Lowest Combined Final Score | -13 | Charleston Shoe Thieves, Dallas Steaks | [Season 19, Day 95](https://reblase.sibr.dev/game/5183b03d-b4ec-4c1f-a38a-e061368011b4)
Highest Combined Runs Scored | 42 | Houston Spies, Hellmouth Sunbeams | [Season 11, Day 23](https://reblase.sibr.dev/game/5ba733fc-5016-45c1-8740-288cc3cce06c)
Highest Combined Final Score | 37 | Charleston Shoe Thieves, Kansas City Breath Mints | [Season 3, Day 57](https://reblase.sibr.dev/game/1f7026c9-1da6-4a4c-b8b8-f417a34af030)
Smallest Score Differential | 0.1 | 11-Way Tie | See [Detail](/records/detail/league/smallest-margin-of-victory)
Greatest Score Differential | 29.8 | Mexico City Wild Wings, Hades Tigers | [Season 19, Day 50](https://reblase.sibr.dev/game/73aaf7cd-aa85-48b0-ac57-31cc4ac55661)
Highest Odds Differential* | 64% (82%-18%) | Chicago Firefighters, Unlimited Tacos | [Season 5, Day 25](https://reblase.sibr.dev/game/a5e63981-9edb-4086-8596-feb0c751375c)
Highest Innings per Inning | 5 | Hades Tigers, Hellmouth Sunbeams | [Season 16, Day 29](https://reblase.sibr.dev/game/cedc165e-128a-4486-872f-3ca5e2917842) Inning 7
Latest Salmon | Inning 16 | | [Season 16, Day 16, Inning 16](https://reblase.sibr.dev/game/a0072ae6-24e4-4cce-bc51-3876394568c8#c3cec96d-87d8-b5fa-1e03-88edb0ec301d)
Most Salmon in One Game | 5 Occurrences | Hades Tigers, Hellmouth Sunbeams | [Season 16, Day 29](https://reblase.sibr.dev/game/cedc165e-128a-4486-872f-3ca5e2917842)
Most Runs Lost from Salmon | 6 | Miami Dale, Hellmouth Sunbeams | [Season 20, Day 84, Inning 4](https://reblase.sibr.dev/game/a22b8277-bcac-456d-9d48-8aca72fedf3c#cc3e170d-f493-fe47-f7e0-b00033b8a37b)
Most Runs Earned before Salmon | 10 | Dallas Steaks, Kansas City Breath Mints | [Season 15, Day 5, Inning 6](https://reblase.sibr.dev/game/9ac134e1-714c-46f8-b069-62ef40b4443d#b75abc3b-1ad4-ab5c-db9f-076e42e6fd3f)

*Needs Verification