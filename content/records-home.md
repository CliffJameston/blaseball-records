---
title: Records
menu: main
weight: 20
---

Note: Only canonical events are recorded here. This means that events such as the Coffee Cup are *not* factored into the records.

If any records are wrong, or you would like to see a new record added, feel free to contact me!

[League-Wide Records](/records/league-records)

[Team Records](/records/team-records)

[Individual Player Records](/records/player-records)

[Miscellaneous Records](/records/misc-records)

[Meta Records](/records/meta-records) (Records about records)

[Record Descriptions](/records/record-descriptions)