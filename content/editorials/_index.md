---
title: Editorials
linkTitle: Editorials
#menu: main
#weight: 40
slug: editorials
---

I don't always have the time to write about new records, but if I do, they'll be here.