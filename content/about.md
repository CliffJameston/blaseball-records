---
title: About
menu: main
weight: 70
---

Blaseball-Records is maintained by Cliff Jameston (Cliff#4877 on Discord).

Significant contributions have been made by:
- Ouch

Blaseball-Records uses the [Minimo](https://minimo.netlify.app/) theme for [Hugo](https://gohugo.io), and is hosted on [GitLab](https://gitlab.com/CliffJameston/blaseball-records).

Icons are sourced from [Game-Icons.net](https://game-icons.net), and created by [Delapouite](https://delapouite.com/).