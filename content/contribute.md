---
title: Contributing
menu: main
weight: 60
---

Blaseball-Records is maintained by Cliff Jameston (Cliff#4877 on Discord).

If you have any corrections (Even one as small as a typo! I can't catch them all, as hard as I try), message or ping me on Discord (I frequent the Tacos side server, but can also be found in the Tacos channels on the main Blaseball server, as well as SIBR's server).

If you would like to submit a new record, we have a [Google Form](https://forms.gle/kLrHt5hDq8vALmvy6) for tracking record requests.

Alternatively, you can contribute by forking the [GitLab repository](https://gitlab.com/CliffJameston/blaseball-records) and submitting a pull request (though this will be left as an exercise for the reader).